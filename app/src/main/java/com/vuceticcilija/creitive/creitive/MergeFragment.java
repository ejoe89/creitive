package com.vuceticcilija.creitive.creitive;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.lang.System.arraycopy;

/**
 * Created by Ilija on 5/17/2017.
 */

public class MergeFragment extends Fragment {

    @BindView(R.id.rvArrays)
    RecyclerView rvArrays;

    private ArrayList<MergeItem> items;
    private LinearLayoutManager llManager;
    private MergeArraysAdapter adapter;

    public MergeFragment() {
    }

    public static MergeFragment newInstance() {
        MergeFragment fragment = new MergeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_merge, container, false);
        ButterKnife.bind(this, rootView);
        generateArrays();

        adapter = new MergeArraysAdapter(getContext(),items);

        adapter.setOnMergeClick(new MergeArraysAdapter.OnMergeItemClick() {
            @Override
            public void onCalculateClick(View v, int position) {
                items.get(position).setResult(merge(items.get(position).getFirstArray(), items.get(position).getSecondArray()));
                adapter.notifyItemChanged(position);
            }
        });

        llManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvArrays.setLayoutManager(llManager);
        rvArrays.setAdapter(adapter);

        return rootView;
    }

    private void generateArrays() {
        items = new ArrayList<>();


        for (int i = 1; i < 10; i++) {

            int[] a = {i * 1, i * 4, i * 9, i * 16};
            int[] b = {i * 9, i * 10, i * 11, i * 19, i * 21};

            MergeItem item = new MergeItem(a, b);
            items.add(item);

            int[] c = {i * 2, i * 7, i * 11};
            int[] d = {i * 5, i * 7, i * 15, i * 14, i * 7};

            MergeItem item1 = new MergeItem(c, d);
            items.add(item1);
        }
    }

    private int[] merge(int[] a, int[] b) {

        int i = 0;
        int j = 0;
        int aLength = a.length;
        int bLength = b.length;
        int totalLength = aLength + bLength;
        int[] c = new int[totalLength];
        int k = 0;

        while (i + j < totalLength) {

            if (i == aLength) {
                c[k++] = b[j++];
                continue;
            }

            if (j == bLength) {
                c[k++] = a[i++];
                continue;
            }

            if (a[i] < b[j]) {
                c[k++] = a[i++];
                continue;
            }

            if (a[i] > b[j]) {
                c[k++] = b[j++];
                continue;
            }

            c[k++] = b[j++];
            i++;
        }

        int[] d = new int[k];
        arraycopy(c, 0, d, 0, k);

        return d;
    }

}
