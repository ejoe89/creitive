package com.vuceticcilija.creitive.creitive;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ilija on 5/17/2017.
 */

public class FactorialFragment extends Fragment {

    public static final String TAG = "DevOps";

    @BindView(R.id.tvFactorial)
    TextView tvNumbers;

    @BindView(R.id.etNumber)
    TextView etNumber;

    public FactorialFragment() {
    }

    public static FactorialFragment newInstance() {
        FactorialFragment fragment = new FactorialFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_factorial, container, false);
        ButterKnife.bind(this, rootView);

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!TextUtils.isEmpty(tvNumbers.getText()))
                    tvNumbers.setText("");
            }
        });
        return rootView;
    }

    @OnClick(R.id.btnFactorial)
    public void onCalculateFactorialClick() {

        String number = etNumber.getText().toString();

        if (!TextUtils.isEmpty(number)) {
            tvNumbers.setText("Factorial of " + number + " is " + factorial(Integer.parseInt(number)));
        }
    }

    private long factorial(int n) {
        if (n < 0) return 0;

        if (n < 2) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }
}
