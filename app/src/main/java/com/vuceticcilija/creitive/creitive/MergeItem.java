package com.vuceticcilija.creitive.creitive;

import java.util.Collections;

/**
 * Created by Ilija on 5/17/2017.
 */

public class MergeItem {

    private int[] firstArray;

    private int[] secondArray;

    private int[] result = null;

    public MergeItem(int[] firstArray, int[] secondArray) {
        this.firstArray = firstArray;
        this.secondArray = secondArray;
    }

    public int[] getFirstArray() {
        return firstArray;
    }

    public void setFirstArray(int[] firstArray) {
        this.firstArray = firstArray;
    }

    public int[] getSecondArray() {
        return secondArray;
    }

    public void setSecondArray(int[] secondArray) {
        this.secondArray = secondArray;
    }

    public void setResult(int[] result) {
        this.result = result;
    }

    public String firstArratToString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < firstArray.length; i++) {
            stringBuilder.append(Integer.toString(firstArray[i]) + ",");
        }

        return stringBuilder.toString();
    }

    public String secondArrayToString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < secondArray.length; i++) {
            stringBuilder.append(Integer.toString(secondArray[i]) + ",");
        }

        return stringBuilder.toString();
    }

    public String resultArrayToString() {

        if(result != null) {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < result.length; i++) {
                stringBuilder.append(Integer.toString(result[i]) + ",");
            }

            return stringBuilder.toString();
        }

        return "result:";
    }

    public boolean isCalculated()
    {
        if (result == null)
            return false;
        else
            return true;
    }

}
