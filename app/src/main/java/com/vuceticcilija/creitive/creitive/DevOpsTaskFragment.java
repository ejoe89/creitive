package com.vuceticcilija.creitive.creitive;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ilija on 5/17/2017.
 */

public class DevOpsTaskFragment extends Fragment {

    public static final String TAG = "DevOps";

    @BindView(R.id.tvNumbers)
    TextView tvNumbers;

    public DevOpsTaskFragment() {
    }

    public static DevOpsTaskFragment newInstance() {
        DevOpsTaskFragment fragment = new DevOpsTaskFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_devops, container, false);
        ButterKnife.bind(this, rootView);

        String number = devOps();
        tvNumbers.setText(number);
        return rootView;
    }

    private String devOps() {

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 1; i < 101; i++) {
            if (i % 15 == 0) {
                Log.d(TAG, "DevOps");
                stringBuilder.append("DevOps\n");
                continue;
            }

            if (i % 3 == 0) {
                Log.d(TAG, "Dev");
                stringBuilder.append("Dev\n");
                continue;
            }

            if (i % 5 == 0) {
                Log.d(TAG, "Ops");
                stringBuilder.append("Ops\n");
                continue;
            }

            stringBuilder.append(Integer.toString(i)+"\n");
        }

        return stringBuilder.toString();
    }
}
