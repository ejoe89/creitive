package com.vuceticcilija.creitive.creitive;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.layer_net.stepindicator.StepIndicator;

import java.lang.reflect.Array;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreitiveActivity extends AppCompatActivity {

    private SectionsPagerAdapter tasksAdapter;
    @BindView(R.id.viewPager)
    ViewPager vpTasks;
    @BindView(R.id.step_indicator)
    StepIndicator stepIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creitive);
        ButterKnife.bind(this);

        tasksAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        vpTasks.setAdapter(tasksAdapter);
        stepIndicator.setupWithViewPager(vpTasks);
        stepIndicator.setClickable(true);
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position)
            {
                case 0:
                    return DevOpsTaskFragment.newInstance();

                case 1:
                    return FactorialFragment.newInstance();

                case 2:
                    return IsPrimeFragment.newInstance();

                case 3:
                    return MergeFragment.newInstance();

                default:
                    return DevOpsTaskFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "SECTION " + (position + 1);
        }
    }
}
