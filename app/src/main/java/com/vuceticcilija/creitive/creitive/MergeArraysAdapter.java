package com.vuceticcilija.creitive.creitive;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilija on 5/17/2017.
 */

public class MergeArraysAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MergeItem> items = new ArrayList<>();
    private Context context;
    private OnMergeItemClick onMergeClick;

    public MergeArraysAdapter(Context context, ArrayList<MergeItem> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_merge_card, parent, false);
        return new MergeHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MergeItem item = items.get(position);
        MergeHolder mergeHolder = (MergeHolder) holder;

        mergeHolder.tvFirstArray.setText(item.firstArratToString());
        mergeHolder.tvSecondArray.setText(item.secondArrayToString());
        mergeHolder.tvThirdArray.setText(item.resultArrayToString());

        if (item.isCalculated())
            mergeHolder.btnCalculate.setVisibility(View.GONE);
        else
            mergeHolder.btnCalculate.setVisibility(View.VISIBLE);


    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    class MergeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvFirstArray;
        TextView tvSecondArray;
        TextView tvThirdArray;
        Button btnCalculate;

        public MergeHolder(View view) {
            super(view);

            tvFirstArray = (TextView) view.findViewById(R.id.tv_first_array);
            tvSecondArray = (TextView) view.findViewById(R.id.tv_second_array);
            tvThirdArray = (TextView) view.findViewById(R.id.tv_result_array);
            btnCalculate = (Button) view.findViewById(R.id.btnCalculate);

            btnCalculate.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onMergeClick != null) {
                switch (view.getId()) {
                    case R.id.btnCalculate:
                        onMergeClick.onCalculateClick(view, getLayoutPosition());
                        break;
                }
            }
        }
    }

    public interface OnMergeItemClick {
        void onCalculateClick(View v, int position);

    }

    public void setOnMergeClick(OnMergeItemClick onMergeClick) {
        this.onMergeClick = onMergeClick;
    }
}
