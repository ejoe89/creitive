package com.vuceticcilija.creitive.creitive;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ilija on 5/17/2017.
 */

public class IsPrimeFragment extends Fragment {

    public static final String TAG = "DevOps";

    @BindView(R.id.tvNumber)
    TextView tvNumbers;

    @BindView(R.id.etNumber)
    TextView etNumber;

    public IsPrimeFragment() {
    }

    public static IsPrimeFragment newInstance() {
        IsPrimeFragment fragment = new IsPrimeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_is_prime, container, false);
        ButterKnife.bind(this, rootView);

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!TextUtils.isEmpty(tvNumbers.getText()))
                    tvNumbers.setText("");
            }
        });
        return rootView;
    }

    private boolean isPrime(int n) {
        if (n == 2)
            return true;

        if (n % 2 == 0)
            return false;

        int div = 3;

        while (div * div <= n) {
            if (n % div == 0)
                return false;

            div += 2;
        }

        return true;
    }

    @OnClick(R.id.btnIsPrime)
    public void onIsPrimeClick() {

        String number = etNumber.getText().toString();

        if (!TextUtils.isEmpty(number)) {
            tvNumbers.setText(isPrime(Integer.parseInt(number)) ? "Number " + number + " is Prime." : "Number " + number + " is Composite");
        }
    }
}
